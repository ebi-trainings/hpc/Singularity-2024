# Welcome to Singularity Containers for HPC Training at EBI

## Getting Started

[Slides Link](https://docs.google.com/presentation/d/1qC_CmvlDy5da5ROiIjp7EkRrbuFjFL0xkI-Qn1wgi4g/edit?usp=sharing)

## Clone Training Repo

**On your Development VM**
```bash
ssh user@<ip_addr>
git clone https://gitlab.ebi.ac.uk/ebi-trainings/hpc/singularity-2024.git
cd singularity-2024
```

**On Codon SLURM Cluster**
```bash
ssh user@codon-slurm-login.ebi.ac.uk
git clone https://gitlab.ebi.ac.uk/ebi-trainings/hpc/singularity-2024.git
cd singularity-2024
```
### Follow training and complete exercies

[Exercises](https://gitlab.ebi.ac.uk/ebi-trainings/hpc/Singularity-2024/-/tree/main/Excercises)


### Useful Links
[Slides Link](https://docs.google.com/presentation/d/1qC_CmvlDy5da5ROiIjp7EkRrbuFjFL0xkI-Qn1wgi4g/edit?usp=sharing)

[Sylabs SingularityCE User Guide](https://docs.sylabs.io/guides/3.11/user-guide/)


[Containers for HPC](https://doit-now.tech/container-technology-for-hpc/)

